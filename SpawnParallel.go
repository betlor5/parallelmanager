package parallelmanager

import (
	"fmt"
	"log"
	"sync"
)

//Transformer ist eine Parallele Routine die input zu Output transformiert
type Transformer func(in interface{}, option interface{}, lg *log.Logger) (out interface{}, status interface{}, errs []error)

//Reducer ist eine Parallele Routine die Input zu einem Output transformiert.
//Der Output muss ein Pointer sein!
type Reducer func(in interface{}, out interface{}, lock *sync.Mutex, option interface{}, lg *log.Logger) (status interface{}, errs []error)

//ErrorHandler ist für das parallele Fehlerhandling.
type ErrorHandler func(errs []error, status interface{}, id string, errStatusChan chan interface{}, errOption interface{}, lg *log.Logger) (quit bool, skip bool, repeat bool)

//SpawnParallel führt parallel mal den doer aus.
//Falls ein Fehler auftritt wird er zum handleError überreicht und dieser kann ggf. alles Stoppen.
//Wenn inChan geschlossen wird und alle doers durchgelaufen sind wird outChan geschlossen.
//Wenn Quit durch den ErrorHandler gesetzt wird, dann inChan entleert.
//
//errStatusChan ermöglicht Fehlercodes herauszugeben
func SpawnParallelTransformer(
	inChan chan interface{}, outChan chan interface{}, option interface{}, errOption interface{}, doer Transformer, handleError ErrorHandler,
	wg *sync.WaitGroup, quit *bool, parallel int, errStatusChan chan interface{}, lg *log.Logger, id string) {

	defer func() {
		wg.Done()
	}()

	open := true
	canSpawn := make(chan bool, parallel)

	wg.Add(1)
	go func() {
		var wgIntern sync.WaitGroup
		for open && !*quit {
			canSpawn <- true
			wgIntern.Add(1)
			//handleDoer muss hier in der for ausgeführt werden, da es auch terminiert wenn der inChan leer und offen ist.
			//Eine solche Situation würde zu einem Deadlock führen.
			go handleTransformer(inChan, outChan, option, errOption, doer, handleError, &wgIntern, canSpawn, quit, &open, errStatusChan, lg, id)
		}
		wgIntern.Wait()
		//Wenn quit = true => ist der autoput egal und somit auch ob es noch laufende Go-Routinen gibt
		//Wenn open = true => die letzte Goroutine die noch Inhalt aus inChan ziehen konnte hat dies getan
		//=>Nach der for kann der Channel geschlossen werden
		msg := fmt.Sprintf("Schliese %v", id)
		fmt.Println(msg)
		lg.Print(msg)
		close(outChan)
	}()
}

//handleTransformer ist für die Koordination zwischen den channels dem doer und dem handleError verantwortlich.
//Die Routine terminiert ggf. auch den gesamten Prozess.
func handleTransformer(inChan chan interface{}, outChan chan interface{}, option interface{}, errOption interface{}, doer Transformer, handleError ErrorHandler,
	wg *sync.WaitGroup, canSpawn chan bool, quitAll *bool, open *bool, errStatusChan chan interface{}, lg *log.Logger, id string) {
	defer func() {
		<-canSpawn
		wg.Done()
	}()

	if *quitAll {
		msg := fmt.Sprintf("%v abbrechen", id)
		lg.Print(msg)
		fmt.Println(msg)
		return
	}
	in, chanOpen := <-inChan
	if !chanOpen {
		*open = false
	}
	if in == nil {
		return
	}
	repeat := true
	msg := fmt.Sprintf("%v startet", id)
	lg.Print(msg)
	fmt.Println(msg)
	var quit, skip bool
	var out interface{}
	var status interface{}
	var errs []error
	for repeat {
		out, status, errs = doer(in, option, lg)
		quit, skip, repeat = handleError(errs, status, id, errStatusChan, errOption, lg)
		if quit {
			msg = fmt.Sprintf("%v abbrechen", id)
			lg.Print(msg)
			fmt.Println(msg)
			*quitAll = true
			for len(inChan) > 0 {
				<-inChan
			}
			return
		}
		if skip {
			msg = fmt.Sprintf("%v überspringen", id)
			lg.Print(msg)
			fmt.Println(msg)
			return
		}
	}

	msg = fmt.Sprintf("%v erfogreich", id)
	lg.Print(msg)
	fmt.Println(msg)
	outChan <- out
}

//SpawnParallelReducer erlaubt es eine Routine zu spanen, die den offenen channel in ein out interface transformiert.
//Es werden solange #parallel Routinen erstellt bis der inChan geschlossen wird oder quit wahr ist.
//Wenn Quit durch den ErrorHandler gesetzt wird, dann inChan entleert.
func SpawnParallelReducer(inChan chan interface{}, out interface{}, option interface{}, errOption interface{}, reduce Reducer, handleError ErrorHandler,
	wg *sync.WaitGroup, quit *bool, parallel int, errStatusChan chan interface{}, lg *log.Logger, id string) {

	defer func() {
		wg.Done()
	}()

	open := true
	canSpawn := make(chan bool, parallel)
	var lock sync.Mutex

	wg.Add(1)
	go func() {
		var wgIntern sync.WaitGroup
		for open && !*quit {
			canSpawn <- true
			wgIntern.Add(1)
			//handleDoer muss hier in der for ausgeführt werden, da es auch terminiert wenn der inChan leer und offen ist.
			//Eine solche Situation würde zu einem Deadlock führen.
			go handleReducer(inChan, out, &lock, option, errOption, reduce, handleError, &wgIntern, canSpawn, quit, &open, errStatusChan, lg, id)
		}
		wgIntern.Wait()
		//Wenn quit = true => ist der autoput egal und somit auch ob es noch laufende Go-Routinen gibt
		//Wenn open = true => die letzte Goroutine die noch Inhalt aus inChan ziehen konnte hat dies getan
		//=>Nach der for kann der Channel geschlossen werden
		msg := fmt.Sprintf("Schliese %v", id)
		fmt.Println(msg)
		lg.Print(msg)
	}()
}

func handleReducer(inChan chan interface{}, out interface{}, lock *sync.Mutex, option interface{}, errOption interface{}, reducer Reducer, handleError ErrorHandler,
	wg *sync.WaitGroup, canSpawn chan bool, quitAll *bool, open *bool, errStatusChan chan interface{}, lg *log.Logger, id string) {
	defer func() {
		<-canSpawn
		wg.Done()
	}()

	if *quitAll {
		msg := fmt.Sprintf("%v abbrechen", id)
		lg.Print(msg)
		fmt.Println(msg)
		return
	}
	in, chanOpen := <-inChan
	if !chanOpen {
		*open = false
	}
	if in == nil {
		return
	}
	repeat := true
	msg := fmt.Sprintf("%v startet", id)
	lg.Print(msg)
	fmt.Println(msg)
	var quit, skip bool
	var status interface{}
	var errs []error
	for repeat {
		status, errs = reducer(in, out, lock, option, lg)
		quit, skip, repeat = handleError(errs, status, id, errStatusChan, errOption, lg)
		if quit {
			msg = fmt.Sprintf("%v abbrechen", id)
			lg.Print(msg)
			fmt.Println(msg)
			*quitAll = true
			for len(inChan) > 0 {
				<-inChan
			}
			return
		}
		if skip {
			msg = fmt.Sprintf("%v überspringen", id)
			lg.Print(msg)
			fmt.Println(msg)
			return
		}
	}

	msg = fmt.Sprintf("%v erfogreich", id)
	lg.Print(msg)
	fmt.Println(msg)
}
